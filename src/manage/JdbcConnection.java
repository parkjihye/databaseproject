package manage;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class JdbcConnection {
		private static final String DB_DRIVER = "org.postgresql.Driver";
		public static void execute (String str, Integer j, String header) throws SQLException{
			
			try {
				Class.forName(DB_DRIVER);
			} catch (ClassNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			Properties props = new Properties();
	
			
			FileInputStream fis = null;
			String filename = "connection.txt";
			
			String line = "";
			String[] tmp;
			String ip = "";
			String dbname = "";
			String schemaname = "";
			String id = "";
			String pw = "";
	
			
			try {
				fis = new FileInputStream(filename);
				FileReader fr = new FileReader(filename);
				BufferedReader br = new BufferedReader(fr);
				
				//-- connection
				for (int i=1; (line = br.readLine()) != null ; i++){
					if(line.indexOf(":") !=-1){
						//System.out.println(i + ":" + line);
						line = line.replaceAll("\\p{Z}", "");
						if(line.contains("IP")){
							tmp = line.split(":");
							ip = tmp[1];
						}else if(line.contains("DB_NAME")){
							tmp = line.split(":");
							dbname = tmp[1];
						}else if(line.contains("SCHEMA_NAME")){
							tmp = line.split(":");
							schemaname = tmp[1];
						}else if(line.contains("ID")){
							tmp = line.split(":");
							id = tmp[1];
						}else {
							tmp = line.split(":");
							pw = tmp[1];
						}
					}
				}
				br.close();
	
				
				
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			/* Setting Connection Info */
			props.setProperty("user", 		id);
			props.setProperty("password", 	pw);
	
			/* Connect! */
			Connection conn = DriverManager.getConnection("jdbc:postgresql://"+ip + ":5432/" + dbname, props);
			conn.setAutoCommit(true);	
			
			Statement st = conn.createStatement();
			
			Integer k = 0;
			if (j == 0){
				
				if(header != ""){
					// -- HEADER 그리는부분
					ResultSet rs_hd = st.executeQuery(header);
					while (rs_hd.next()) {
						System.out.print(" " + rs_hd.getString(1) + " ");
						k++;
					}
					System.out.println();
					rs_hd.close();

					ResultSet rs = st.executeQuery(str);
					while (rs.next()) {
						for (int i = 1; i <= k ; i ++){
						    System.out.print(" " + rs.getString(i) + " ");
						}
						System.out.println();
					}
					rs.close();
				}else{
					st.executeUpdate(str);	
					//st.executeQuery(str);
				}
				
			}else{
				// -- COLUMN 값이 동적일때
				ResultSet rs = st.executeQuery(str);
				while (rs.next()) {
					for (int i = 1; i <= j ; i ++){
					    System.out.print(" " + rs.getString(i) + " ");
					}
					System.out.println();
				}
				rs.close();
			}
			
			st.close();
			conn.close();
			return;
		
	}
		
		
		
}
