package manage;

import java.util.Scanner;

public class Utils {
	
	// -- 공백 모두제거
	public static String replaceNull(String str){
		
		str = str.replaceAll("\\p{Z}", "");
		return str;
		
	}
	
	public static String replaceSENull(String str){
		
		str =str.replaceAll("(^\\p{Z}+|\\p{Z}+$)", "");
		return str;
		
	}
	
	public static String upper (String str){
		
		str = str.toUpperCase();
		return str;
		
	}
	
	public static String lower (String str){
		
		str = str.toLowerCase();
		return str;
		
	}
	
	public static String input (){
        Scanner sysin = new Scanner(System.in);      
        String sysval = sysin.nextLine(); 
        sysval = Utils.replaceNull(sysval);
		return sysval;
		
	}
}
