package manage;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Scanner;
import manage.Utils;

public class ProjectMain {
	private static Scanner menu;
	private static Scanner csv;
	private static BufferedReader br;

	public static void main(String args[]) throws ClassNotFoundException, SQLException, IOException {
		
			
		//-- main menu choice
        System.out.print("Please input the instruction number (1: Import from CSV, 2: Manipulate Data, 3: Exit) :");

        menu = new Scanner(System.in);      
        String no = "";
        no = menu.nextLine(); 
        no = Utils.replaceNull(no);

		
        if (no.equals("1")){
        	try{
	    		importTXT();	// create a table using the test.txt
	        	importCSV();	// insert data using the test.csv
        	}finally {
				main(null);
			}
        }else if (no.equals("2")){	//manipulate
        	manipulateData();
        	
        }else if (no.equals("3")){	//exit
        	System.exit(1);

        }else {
        	//System.exit(0);
        	System.out.println("try again!");
        	main(null);
        }
		
    }
	
	
	
	//-- create a table using the test.txt
	public static void importTXT() throws ClassNotFoundException, SQLException, IOException{
		System.out.println("[Import from CSV]");
		System.out.print("Please specify the filename for table description : ");
        String tablefilename = Utils.input();
        String tablename = "";
        String colname = "";
        String type = "";
        String isPK = "";
        String isNotNull = "";
        String[] tmp;
        String query = "";
        String[] colval = null;
        String[] typeval = null;
        
        FileInputStream fis = null;
		try {
			fis = new FileInputStream(tablefilename);
			FileReader fr = new FileReader(tablefilename);
			BufferedReader br = new BufferedReader(fr);
			
			String col = "";
			for (int i=1; (col = br.readLine()) != null ; i++){
				col = Utils.replaceNull(col);
				if(col.indexOf(":") !=-1){
					if(col.contains("NAME")){
						tmp = col.split(":");
						tablename = tmp[1];		//get a table name
					}else if(col.contains("PK")){
						tmp = col.split(":");
						isPK = tmp[1];			//get primary keys
					}else if(Utils.replaceNull(Utils.upper(col)).contains("NOTNULL")){
						tmp = col.split(":");
						isNotNull = tmp[1];		//get not nulls
					}else if(col.contains("Column") && !(col.contains("DataType"))){
						tmp = col.split(":");
						if (colname.equals("")){
							colname = tmp[1];
						}else{
							colname = colname + ":" + tmp[1];	//get columns
						}
					}else if(col.contains("DataType")){
						tmp = col.split(":");
						if (type.equals("")){
							type = tmp[1];
						}else{
							type = type + ":" + tmp[1];			//get datatypes
						}
					}else{
						
					}
				}
			}
			colval = colname.split(":");
			typeval = type.split(":");
			
			String colquery = "";

			for (int k =0; k < colval.length; k++){		//make columns, types and notnull or not
				colquery = colquery + colval[k] + " " + typeval[k];
				if (isNotNull.contains(colval[k])){
					colquery = colquery + " not null,";
				}else{
					colquery = colquery + ",";
				}
			}
			
			//make query
			query = "CREATE TABLE " + tablename + " (" +
					colquery +
					" primary key (" + isPK+ "))";
			
			System.out.println(query);
			
			//execute create query
			JdbcConnection.execute(query, 0, "");
			System.out.println("Table is newly created as described in th file.");
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			
			//no filename
			System.out.println("file not found! check the filename");
			main(null);
			
			//e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(" error message :: " + e.getMessage());
			main(null);
			//System.out.println(" error code :: " + e.getErrorCode());
			//e.printStackTrace();
		}

		
		
		return;
        
		
	}
	
	
	//-- insert data using the test.csv
	private static void importCSV() throws ClassNotFoundException, SQLException, IOException {

		String insertquery = "";
		String[] tmp;
		int i = 0;		//-- 파일내 총 line수 
		int l= 0; 		//-- insert 수
		int err = 0;	//-- err 수
		
		System.out.print("Please specify the CSV filename : ");	
		String csvfilename = Utils.input();
		
		
		try {
			FileReader fr = new FileReader(csvfilename);
			br = new BufferedReader(fr);
			
			String col = "";
			
			
			for (i=1 ; (col = br.readLine()) != null ; i++){ // csv파일의 맨 첫줄 제외하려고 i=1 부터..
				String context = "";	//columns
				col = Utils.replaceNull(col);
				if(col.indexOf(",") !=-1){
						tmp = col.split(",");
						//make insert query
						for (l=0; l<tmp.length; l++){
							if (context.equals("")){
								context = "'" + tmp[l] + "'";
							}else{
								context = context + ", '"+ tmp[l] + "'";
							}
						}
						
						insertquery = "INSERT INTO test values(" + context + ")";
						System.out.println(insertquery);
						
						//execute insert query
						JdbcConnection.execute(insertquery, 0, "");
				}
			}
			
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("file not found! check the filename");
			main(null);
			//e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			err = err+1;
			System.out.println(" error message :: " + e.getMessage());
			System.out.println(" error code :: " + e.getErrorCode());
			//importCSV(csvfilename);
		
			//e.printStackTrace();
		} finally {
			System.out.println("Data import completed. (Insertion Success : "+ l + ", Insertion Failure : " + err);

		}
		
		return;
		
	}


	
	private static void manipulateData() {
		// TODO Auto-generated method stub
		System.out.println("[Manipulate Data]");
		System.out.print("Please input the instruction number (1:Show Tables, 2:Describe Table, 3:Select, 4:Drop Table, 5:Back to main) : ");
		
        Scanner submenu = new Scanner(System.in);      
        
        String no = "";
        no = submenu.nextLine(); 
        no = Utils.replaceNull(no);
        String tbquery = "";
        String tblist = "";
        Integer i = 0;
        
        if (no.equals("1")){
        	System.out.println("==========");
        	System.out.println("Table List");
        	System.out.println("==========");
        	// -- select tablename from pg_tables where schemaname = 'public';
        	tbquery = "select tablename from pg_tables where schemaname = 'public'";
        	try {
        		JdbcConnection.execute(tbquery, 1, "");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				manipulateData();
			}

        }else if (no.equals("2")){
    		System.out.print("Please specify the table name : ");
            Scanner tbnamein = new Scanner(System.in);      
            
            String tbname = "";
            tbname = tbnamein.nextLine(); 
            tbname = Utils.replaceNull(tbname);
            String specifytb = "";
            System.out.println("==================================================================================");
            System.out.println("Column Name | Data Type | Character Maximun Length(or Numeric Precision and Scale)");
            System.out.println("==================================================================================");
            // -- select column_name,data_type,character_maximum_length,numeric_precision,numeric_scale  from information_schema.columns where table_name = 'GRADE' order by collation_name
            specifytb = "select column_name,data_type,character_maximum_length,numeric_precision,numeric_scale  from information_schema.columns "
            		    + "where table_name = '"+ tbname +"' "
            		    + "order by collation_name";
        	try {
        		JdbcConnection.execute(specifytb, 5, "");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				manipulateData();
			}
        	
        }else if (no.equals("3")){
        	// -- SELECT ID, name, address, department_ID FROM student_table"
    		System.out.print("Please specify the table name : ");
            Scanner tbnamein = new Scanner(System.in);      
            String tbname = "";
            String allcol = "";
            tbname = tbnamein.nextLine(); 
            tbname = Utils.replaceNull(tbname);
            
            System.out.print("Please specify column which you want to retrieve (ALL : *) : ");
            Scanner colin = new Scanner(System.in);  
            String allcol_tmp = colin.nextLine();
            String colname = "";
            Integer colcnt = 0;
            if (allcol_tmp.contains("*")){
            	allcol = "*";
                // -- table headers
                colname = "SELECT column_name FROM information_schema.columns "
                		+ "where table_name = '" + tbname + "'";
            }else{
            	
            	String[] col = allcol_tmp.split(",");
            	colcnt = col.length;
                //col = colin.nextLine(); 
                //col = Utils.replaceNull(col);
                for (i=0; i < colcnt; i++){
                	col[i] = "\"" + Utils.replaceSENull(col[i]) + "\"";
                	if (i == 0){
                		allcol = allcol + col[i];
                	}else{
                		allcol = allcol + "," + col[i];
                	}
                	
                }
                
            }
            // -- select query
            String selectquery = "SELECT " + allcol + " FROM \"" + tbname + "\"";
            
            String condiquery = makeCondition();
            
            if (!condiquery.equals("")){
            	selectquery = selectquery  + " WHERE" + condiquery;
            }else{
            	
            }
            

            
            
            
            System.out.print("Please specify the column name for ordering (Press enter : skip) : ");
            Scanner orderin = new Scanner(System.in);      
            String order = "";
            order = orderin.nextLine(); 
            order = Utils.replaceNull(order);
            String criteria = "";

            if (order.equals("")){

            }else{
                System.out.print("Please specify the sorting criteria (Press enter : skip) : ");
                Scanner critin = new Scanner(System.in);      

                criteria = critin.nextLine(); 
                criteria = Utils.replaceNull(criteria);
            }
            

            

            


            
            
        	if (!order.equals("")){
        
        		selectquery = Utils.replaceSENull(selectquery) + " order by \"" + order + "\"";
        	}
        	else{
        			
        	}
        	
        	if (criteria != ""){
            	selectquery = Utils.replaceSENull(selectquery) + " " + Utils.replaceSENull(criteria);
            }else{
            	
            }
            	         
            
            
            
            
            System.out.println(selectquery);
            System.out.println(colname);
            try {
            	if (colcnt == 0){
            		System.out.println("=========================================================");
            		JdbcConnection.execute(selectquery,colcnt, colname);
            		System.out.println("=========================================================");

            	}else {
            		System.out.println("=========================================================");
            		System.out.println(allcol.replaceAll("\"", "|"));
            		System.out.println("=========================================================");
            		JdbcConnection.execute(selectquery, colcnt, "");
            	}
            	
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
			} finally {
				//-- 서브메뉴로 돌아감
				manipulateData();
			}
        	
        }else if (no.equals("4")){
    		System.out.print("Please specify the table name : ");
            String tbname = Utils.input();
            System.out.print("If you delete this table, it is not guaranteed to recover again. Are you sure you want to delete this table (Y: yes, N: no)?");
            String yn = Utils.input();
            if (Utils.upper(yn).equals("Y")){
                // -- DROP TABLE test
                String dropquery = "DROP TABLE " + tbname;
                
            	try {
            		JdbcConnection.execute(dropquery, 0, "");
            		System.out.println("<The table "+tbname+" is deleted>");
    			} catch (SQLException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			} finally {
    				manipulateData();
    			}
            }else{
            	manipulateData();
            }

            
        	
        }else {
        	try {
				main(null);
			} catch (ClassNotFoundException | SQLException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	
        }
        
        
        
        
	}

	private static String makeCondition() {
		// TODO Auto-generated method stub

        System.out.print("Please specify the column which you want to make condition (Press enter : skip) : ");
//        Scanner condin = new Scanner(System.in);      
        String condition = Utils.input();
//        condition = condin.nextLine(); 
//        condition = Utils.replaceNull(condition);
        String opvalue = "";
        String operation = "";
        String condistr = "";
        if (condition.equals("")){

        }else{
        	System.out.print("Please specify the condition (1: =, 2: >, 3: <, 4: >=, 5: <=, 6: !=, 7: LIKE) :");
        	String operation_in = Utils.input();
        	
        	switch (operation_in) {
        	 case "1" :
        		 operation = "=";
        	  break;
        	 case "2" :
        		 operation = ">";
        	  break;
        	 case "3" :
        		 operation = "<";
        	  break;
        	 case "4" :
        		 operation = ">=";
           	  break;
        	 case "5" :
        		 operation = "<=";
           	  break;
        	 case "6" :
        		 operation = "!=";
           	  break;
        	 case "7" :
        		 operation = "LIKE";
           	  break;
        	 default :
        		 operation = "";
        	  break;
        	}


        	//System.out.println("############# :: " + operation);
        	System.out.print("Please specify the condition value (" + condition + " " + operation + " ?) :");
        	opvalue = Utils.input();
        	        	
        }
        
        //-- condition
        if (!condition.equals("")){
        	if(operation.equals("LIKE")){
        		condistr = condistr + " \"" +Utils.replaceSENull(condition) + "\""
            			+" "+ operation +" "+ "'%" + opvalue + "%'";
        	}else{
        		condistr = condistr + " \"" +Utils.replaceSENull(condition) + "\""
        			+ operation + "'" + opvalue + "'";
        	}
        }else{
        	
        }
        
    	System.out.print("Please specify the condition (1: AND, 2: OR, 3: finish) : ");
    	String nextcondi = Utils.input();
    	if (nextcondi.equals("1")){
    		condistr = condistr + " AND " + makeCondition();
    		
    	}else if (nextcondi.equals("2")){
    		condistr = condistr + " OR " + makeCondition();
    	}else{
    		
    	}
        
    	return condistr;
        
	}
	
}







/////////////////////////
///참고 예제
////////////////////////////
//Statement st = conn.createStatement();
//Statement st2 = conn.createStatement();
//
///* Create Table SQL */
//String CreateTableSQL = "CREATE TABLE student_table " +
//						"(ID int, " +
//						"name varchar(20) not null, " +
//						"address varchar(50) not null," +
//						"department_ID int," +
//						"primary key (ID))";
//
///* Drop table */
//String DropTableSQL = "DROP TABLE test";
////st.executeUpdate(DropTableSQL);
//st.executeUpdate(createquery);
//
////////--- start				
////		
/////* Insert Row using Statement */
////String InsertSQL_1 = "INSERT INTO student_table values(1, 'Brandt', 'addr1', 1)";
//st.executeUpdate(insertquery);
//
///* Insert Row using PreparedStatement */
//String InsertSQL_2 = "INSERT INTO student_table (ID, name, address, department_ID) values(?, ?, ?, ?)";
//
//PreparedStatement preparedStmt = conn.prepareStatement(InsertSQL_2);
//preparedStmt.setInt(1, 2);
//preparedStmt.setString(2, "Chavez");
//preparedStmt.setString(3, "addr2");
//preparedStmt.setInt(4, 2);
//
//preparedStmt.execute();
//ResultSet rs = st.executeQuery("SELECT ID, name, address, department_ID FROM student_table");
//
//System.out.println("============ RESULT ============");
//while (rs.next()) {
//    System.out.print("ID : " + rs.getString(1) + ", ");
//    System.out.print("Name : " + rs.getString(2) + ", ");
//    System.out.print("Address : " + rs.getString(3) + ", ");
//    System.out.print("Department_ID : " + rs.getString(4));
//    System.out.println();
//}
//
///* Update Row */
//String UpdateSQL = "UPDATE student_table SET address = ? where ID = ?";
//
//preparedStmt = conn.prepareStatement(UpdateSQL);
//preparedStmt.setString(1, "addr3");
//preparedStmt.setInt(2, 2);		
//preparedStmt.executeUpdate();
//
//rs = st.executeQuery("SELECT ID, name, address, department_ID FROM student_table");
//
//System.out.println("============ RESULT ============");
//while (rs.next()) {
//    System.out.print("ID : " + rs.getString(1) + ", ");
//    System.out.print("Name : " + rs.getString(2) + ", ");
//    System.out.print("Address : " + rs.getString(3) + ", ");
//    System.out.print("Department_ID : " + rs.getString(4));
//    System.out.println();
//}
///* Delete Row */
//String DeleteSQL = "DELETE FROM student_table where ID = 2";
//st.executeUpdate(DeleteSQL);
//
//rs = st.executeQuery("SELECT ID, name, address, department_ID FROM student_table");
//
//System.out.println("============ RESULT ============");
//while (rs.next()) {
//    System.out.print("ID : " + rs.getString(1) + ", ");
//    System.out.print("Name : " + rs.getString(2) + ", ");
//    System.out.print("Address : " + rs.getString(3) + ", ");
//    System.out.print("Department_ID : " + rs.getString(4));
//    System.out.println();
//}
//
//System.out.println("============ RESULT ============");
//
//    
//    String tablename;
//    Scanner scan = new Scanner(System.in);      
//    
//    System.out.println("테이블명 입력하세요 :");
//    
//    tablename = scan.nextLine();            
//    
//    System.out.println("테이블명 : \""+ tablename + "\"");
//    
//    
//	ResultSet table = st.executeQuery("SELECT * FROM " +  tablename);
//
//	System.out.println("============ RESULT ============");
//	while (table.next()) {
//	    System.out.print("ID : " + table.getString(1) + ", ");
//	    System.out.print("Name : " + table.getString(2) + ", ");
//	    System.out.print("Address : " + table.getString(3) + ", ");
//	    System.out.print("Department_ID : " + table.getString(4));
//	    System.out.println();
//	}
//
//	System.out.println("============ RESULT ============");

/////////////----end

/* Drop table */
//String DropTableSQL = "DROP TABLE student_table";
//st.executeUpdate(DropTableSQL);

//preparedStmt.close();
//	st.close();
//	rs.close();




